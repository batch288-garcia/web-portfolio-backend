const Profile = require("../models/profile.js");

// Create Profile object
module.exports.addProfile = (request, response) => {
  Profile.findOne({
    firstName: request.body.firstName,
    lastName: request.body.lastName,
    email: request.body.email,
  })
    .then((result) => {
      if (result) {
        return response.send("Sorry Duplicate record has been found.");
      } else {
        let newProfile = new Profile({
          firstName: request.body.firstName,
          lastName: request.body.lastName,
          email: request.body.email,
          contactNum: request.body.contactNum,
          address: request.body.address,
          title: request.body.title,
          landingDescription: request.body.landingDescription,
          homeDescription: request.body.homeDescription,
        });
        newProfile;
        newProfile
          .save()
          .then((save) => response.send("successfully added new Profile"))
          .catch((error) =>
            response.send("Sorry! unable to add new profile please try again.")
          );
      }
    })
    .catch((error) =>
      response.send("Sorry! unable to add new profile please try again.")
    );
};

// Get active profiles
module.exports.getActiveProfile = (request, response) => {
  Profile.find({ isActive: true })
    .then((result) => {
      if (result.length === 0) response.send("No active profile");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error."));
};

// Get deactive profiles
module.exports.getDeacticatedProfile = (request, response) => {
  Profile.find({ isActive: false })
    .then((result) => {
      if (result.length === 0) response.send("No deactivated profile");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error."));
};

// Get profiles
module.exports.getProfiles = (request, response) => {
  Profile.find({})
    .then((result) => {
      if (result.length === 0) response.send("No profiles have been found");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error"));
};

// Update Profile details
module.exports.updateProfileDetails = (request, response) => {
  const profileId = request.params.profileId;

  let updatedProfile = {
    firstName: request.body.firstName,
    lastName: request.body.lastName,
    email: request.body.email,
    contactNum: request.body.contactNum,
    address: request.body.address,
    title: request.body.title,
    landingDescription: request.body.landingDescription,
    homeDescription: request.body.homeDescription,
    $inc: { __v: 1 },
  };

  Profile.findByIdAndUpdate(profileId, updatedProfile)
    .then((result) => {
      if (result) {
        response.send("Successfully updating profile!");
      } else {
        response.send("Unable to update profile.");
      }
    })
    .catch((error) => response.send("Catch Error!"));
};

// Archive Profile
module.exports.archiveProfile = (request, response) => {
  const profileId = request.params.profileId;

  let deactivateProfile = {
    isActive: request.body.isActive,
  };

  Profile.findById(profileId)
    .then((result) => {
      if (result.isActive === true && deactivateProfile.isActive === false) {
        Profile.findByIdAndUpdate(profileId, deactivateProfile).then((result) =>
          response.send("Profile has been deactivated.")
        );
      } else if (
        result.isActive === false &&
        deactivateProfile.isActive === false
      ) {
        return response.send("Sorry, this profile is already deactivated");
      } else {
        return response.send(
          "sorry this route is for deactivation only, can not activate profile"
        );
      }
    })
    .catch((error) => response.send("Catch Error"));
};

// unArchive Profile
module.exports.unArchiveProfile = (request, response) => {
  const profileId = request.params.profileId;

  let activateProfile = {
    isActive: request.body.isActive,
  };

  Profile.findById(profileId)
    .then((result) => {
      if (result.isActive === false && activateProfile.isActive === true) {
        Profile.findByIdAndUpdate(profileId, activateProfile).then((result) =>
          response.send("Profile has been activated.")
        );
      } else if (
        result.isActive === true &&
        activateProfile.isActive === true
      ) {
        return response.send("Sorry, this profile is already active");
      } else {
        return response.send(
          "sorry this route is for activating profile only, can not deactivate profile"
        );
      }
    })
    .catch((error) => response.send("Catch Error"));
};
