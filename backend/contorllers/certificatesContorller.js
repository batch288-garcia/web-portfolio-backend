const { request, response } = require("express");
const Certificates = require("../models/certificates.js");

// Create Certificate details
module.exports.addCertificate = (request, response) => {
  Certificates.findOne({ title: request.body.title })
    .then((result) => {
      if (result) {
        response.send("Sorry, Duplicate record has been found");
      } else {
        const newCertificate = new Certificates({
          title: request.body.title,
          description: request.body.description,
          imgURL: request.body.imgURL,
          isActive: request.body.isActive,
        });
        newCertificate
          .save()
          .then((save) => response.send("Successfully added new Certificate."))
          .catch((error) => response.send("Saving catch error: " + error));
      }
    })
    .catch((error) => response.send("Catch error as a whole: " + error));
};
// Retrive Active Certificates
module.exports.getActiveCertificate = (request, response) => {
  Certificates.find({ isActive: true })
    .then((result) => {
      if (result.length === 0) response.send("No Active Certificates found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive Deactivated Certificates
module.exports.getDeactivatedCertificates = (request, response) => {
  Certificates.find({ isActive: false })
    .then((result) => {
      if (result.length === 0)
        response.send("No Deactivated Certificates found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive All Certificate details
module.exports.getCertificate = (request, response) => {
  Certificates.find({})
    .then((result) => {
      if (result.length === 0) response.send("No Certificates found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Update Certificate details
module.exports.updateCertificate = (request, response) => {
  const certificateId = request.params.certificateId;

  const updatedCertificate = {
    title: request.body.title,
    description: request.body.description,
    imgUrRL: request.body.imgURL,
  };

  Certificates.findByIdAndUpdate(certificateId, updatedCertificate)
    .then((result) => {
      if (result) response.send("Successfully updating Certificate");
      else
        response.send("Sorry, unable to update Certificate, please try again.");
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Delete/Archive Certificate details
module.exports.archiveCertificate = (request, response) => {
  const certificateId = request.params.certificateId;

  const deactivateCertificate = {
    isActive: request.body.isActive,
  };

  Certificates.findById(certificateId)
    .then((result) => {
      if (
        result.isActive === true &&
        deactivateCertificate.isActive === false
      ) {
        Certificates.findByIdAndUpdate(
          certificateId,
          deactivateCertificate
        ).then(() => response.send("Successfully deativating Certificate."));
      } else if (
        result.isActive === false &&
        deactivateCertificate.isActive === false
      ) {
        response.send("This certificate is already deactivated");
      } else
        response.send(
          "Sorry this route is for deactivation only, cannot activate. Thanks!"
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Un-Archive Certificate details
module.exports.unArchiveCertificate = (request, response) => {
  const certificateId = request.params.certificateId;

  const activateCertificate = {
    isActive: request.body.isActive,
  };

  Certificates.findById(certificateId)
    .then((result) => {
      if (result.isActive === false && activateCertificate.isActive === true) {
        Certificates.findByIdAndUpdate(certificateId, activateCertificate).then(
          () => response.send("Successfully activating certificate.")
        );
      } else if (
        result.isActive === true &&
        activateCertificate.isActive === true
      ) {
        response.send("This certificate is already active.");
      } else
        response.send(
          "Sorry this route is for activating certificate only, cannot deactivate. Thank!"
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};
