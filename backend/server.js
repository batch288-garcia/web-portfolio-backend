const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const profileRoutes = require("./routes/profileRoutes.js");
const backgroundRoutes = require("./routes/backgroundRoutes.js");
const skillsRoutes = require("./routes/skillsRoutes.js");
const certificatesRoutes = require("./routes/certificateRoutes.js");
const projectsRoues = require("./routes/projectRoutes.js");

const port = 1001;

const app = express();

mongoose.connect(
  "mongodb+srv://admin:admin@batch288garcia.ltipebr.mongodb.net/WebPortFolioAPI?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

let database = mongoose.connection;

database.on(
  "error",
  console.error.bind(console, `Error, unable to connect to Database!`)
);

database.once("open", () =>
  console.log(`Congratulations! you are now connected to our Database!`)
);

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(
  cors({
    origin: [],
    methods: ["POST", "GET", "PATCH"],
    credentials: true,
  })
);

// default for checking in vercel
app.get("/", (req, res) => {
  res.send("Hello Vercel!");
});

// routes here
// route for profile
app.use("/profile", profileRoutes);

// route for background
app.use("/background", backgroundRoutes);

// route for skills
app.use("/skills", skillsRoutes);

// route for certificates
app.use("/certificates", certificatesRoutes);

// route for projects
app.use("/projects", projectsRoues);

app.listen(port, () => {
  console.log(`Server is running @ port ${port}`);
});
