const mongoose = require("mongoose");

const profileSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "Profile first name is required"],
  },
  lastName: {
    type: String,
    required: [true, "Profile last name is required"],
  },
  email: {
    type: String,
    required: [true, "Profile email is required"],
  },
  contactNum: {
    type: Number,
    required: [true, "Profile contact number is required"],
  },
  address: {
    type: String,
    required: [true, "Profile address is required"],
  },
  title: {
    type: String,
    required: [true, "Profile title is required"],
  },

  profilePicture: {
    type: String,
    default:
      "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
  },

  landingDescription: {
    type: String,
    required: [true, "Profile lading description is required"],
  },
  homeDescription: {
    type: String,
    required: [true, "Profile home description is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

const Profile = new mongoose.model("Profile", profileSchema);

module.exports = Profile;
