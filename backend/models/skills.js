const mongoose = require("mongoose");

const {
  programmingSkillSchema,
  softSkillSchema,
  additionalSkillsSchema,
} = require("./skillsSchemas.js");

const skillsSchema = new mongoose.Schema({
  programming: programmingSkillSchema,
  softSkills: softSkillSchema,
  additionalSkills: additionalSkillsSchema,
});

const Skills = new mongoose.model("Skills", skillsSchema);

module.exports = Skills;
