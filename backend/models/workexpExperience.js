const mongoose = require("mongoose");

const workExpirienceSchema = new mongoose.Schema({
  company: {
    type: String,
    required: [true, "Work title is required"],
  },
  workTitle: {
    type: String,
    required: [true, "Work title is required"],
  },
  workDuration: {
    type: String,
    required: [true, "Work Duration is required"],
  },
  workTask: [
    {
      task: {
        type: String,
        required: [true, "Work task is required"],
      },
    },
  ],
  isActive: {
    type: Boolean,
    default: true,
  },
});

const WorkExperience = new mongoose.model(
  "WorkExperience",
  workExpirienceSchema
);

module.exports = WorkExperience;
