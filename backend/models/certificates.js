const mongoose = require("mongoose");

const cetificatesSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Certificate title is required"],
  },
  description: {
    type: String,
    required: [true, "Certificate description is required"],
  },
  imgURL: {
    type: String,
    default:
      "https://media.istockphoto.com/id/1199906477/vector/image-unavailable-icon.jpg?s=170667a&w=0&k=20&c=QRaXTJuDrWe8Mwi-w98RHoy8-TSdbFPaYFeyUqLidds=",
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

const Certificate = new mongoose.model("Certificates", cetificatesSchema);

module.exports = Certificate;
