const mongoose = require("mongoose");

const programmingSkillSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, "Skill Title is required"],
  },
  description: {
    type: String,
    required: [true, "Description is requred"],
  },
  rating: {
    type: Number,
    require: [true, "Star is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

const softSkillSchema = new mongoose.Schema({
  skillName: {
    type: String,
    required: [true, "soft skill is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

const additionalSkillsSchema = new mongoose.Schema({
  skillName: {
    type: String,
    required: [true, "Additional skill is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

module.exports = {
  programmingSkillSchema,
  softSkillSchema,
  additionalSkillsSchema,
};
