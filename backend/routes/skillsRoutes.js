const express = require("express");
const skillsController = require("../contorllers/skillsController.js");
const Skills = require("../models/skills.js");

const router = express.Router();

// Router for Add Programming Skill
router.post("/AddProgrammingSkill", skillsController.addProgrammingSkill);

// Router for Add Soft Skill
router.post("/AddSoftSkill", skillsController.addSoftSkill);

// Router for Add Soft Skill
router.post("/AddAdditionalSkill", skillsController.addAddtionalSkill);

// Router for retreiving Active Programming Skill
router.get("/GetProgrammingSkill", skillsController.getActiveProgrammingSKill);

// Router for retreiving Active Soft Skill
router.get("/GetSoftSkill", skillsController.getActiveSoftSkill);

// Router for retreiving Active Additional Skill
router.get("/GetAdditionalSkill", skillsController.getActiveAdditionalSkill);

// Router for retrieving Deactivated Programmign Skill
router.get(
  "/GetDeactivatedProgSkills",
  skillsController.getDeactivatedProgSkills
);

// Router for retreiving Deactiated Soft Skills
router.get(
  "/GetDeactivatedSoftSkills",
  skillsController.getDeactivatedSoftSkills
);

// Router for retrieving Deactivated Additional Skills
router.get(
  "/GetDeactivatedAddSkills",
  skillsController.getDeactivatedAddSkills
);

// Ruter for Updating Programming Skills
router.patch(
  "/:programmingSkillId/UpdateProgrammingSkill",
  skillsController.updateProgrammingSKill
);

// Router for Updating Soft Skills
router.patch("/:softSkillId/UpdateSoftSkill", skillsController.updateSoftSkill);

// Router for Updating Additional Skills
router.patch(
  "/:addSkillId/UpdateAdditionalSkill",
  skillsController.updateAdditionalSkill
);

// Router for Archiving Programming Skill
router.patch(
  "/:programmingSkillId/ArchiveProgrammingSkill",
  skillsController.archiveProgrammingSkill
);

// Router for Archiving Soft Skill
router.patch(
  "/:softSkillId/ArchiveSoftSkill",
  skillsController.archiveSoftSkill
);

// Router for Archiving Additional Skill
router.patch(
  "/:addSkillId/ArchiveAdditionalSkill",
  skillsController.archiveAdditionalSkill
);

// Router for Un-Archive Programming Skill
router.patch(
  "/:programmingSkillId/UnArchiveProgrammingSkill",
  skillsController.unArchiveProgrammingSkill
);

// Router for Un-Archive Soft Skill
router.patch(
  "/:softSkillId/UnArchiveSoftSkill",
  skillsController.unArchiveSoftSkill
);

// Router for Un-Archive Additional Skill
router.patch(
  "/:addSkillId/UnArchiveAdditionalSkill",
  skillsController.unArchiveAdditionalSkill
);

module.exports = router;
