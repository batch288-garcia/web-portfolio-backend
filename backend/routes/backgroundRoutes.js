const express = require("express");
const backgroundController = require("../contorllers/backgroundController.js");

const router = express.Router();

// Router for Add Education
router.post("/AddEducation", backgroundController.addEducation);

// Router for Add WorkExpi
router.post("/AddWorkExperience", backgroundController.addWorkExperience);

// Route for getting Active Education
router.get("/GetActiveEducation", backgroundController.getActiveEducation);

// Route for retrieve deactivated Education
router.get(
  "/GetDeactivatedEducation",
  backgroundController.getDeactivatedEducation
);

// Router for retrieve all education
router.get("/GetEducation", backgroundController.getEducation);

// Router for get Active Work Expi
router.get(
  "/GetActiveWorkExperience",
  backgroundController.getActiveWorkExperience
);

// Router for get Deactivated WorkExpi
router.get(
  "/GetDeactivatedWorkExperience",
  backgroundController.getDeactivatedWorkExperience
);

// Router for getting all WorkExperience
router.get("/GetWorkExperience", backgroundController.getWorkExperience);

// Router for updating education
router.patch(
  "/:educationId/UpdateEducation",
  backgroundController.updateEducation
);

// Route for updating workExpi
router.patch(
  "/:workExpiId/UpdateWorkExpi",
  backgroundController.updateWorkExperience
);

// Route for Archive Education
router.patch(
  "/:educationId/ArchiveEducation",
  backgroundController.archivedEducation
);

// Router for Archive WorkExperience
router.patch(
  "/:workExpiId/ArchiveWorkExperience",
  backgroundController.archiveWorkExperience
);

// Router for Un-Archive Education
router.patch(
  "/:educationId/UnArchiveEducation",
  backgroundController.unArchiveEducation
);

// Router for Un-Archive WorkExpi
router.patch(
  "/:workExpiId/UnArchiveWorkExperience",
  backgroundController.unArchiveWorkExperience
);

module.exports = router;
