const express = require("express");
const certificateController = require("../contorllers/certificatesContorller");

const router = express.Router();

// Router for adding Certificate
router.post("/AddCertificate", certificateController.addCertificate);

// Router for Retriveing Certificate
// Retrieving Active Certificates
router.get(
  "/GetActiveCertificates",
  certificateController.getActiveCertificate
);

// Retrieving Deactivated Certificates
router.get(
  "/GetDeactivatedCertificates",
  certificateController.getDeactivatedCertificates
);

// Retirieving All Certificates
router.get("/GetCertificates", certificateController.getCertificate);

// Router for Updating Certificate
router.patch(
  "/:certificateId/UpdateCertificate",
  certificateController.updateCertificate
);

// Router for Archiving Certificate]
router.patch(
  "/:certificateId/ArchiveCertificate",
  certificateController.archiveCertificate
);

// Router for Un-Archiving Certificate]
router.patch(
  "/:certificateId/UnArchiveCertificate",
  certificateController.unArchiveCertificate
);

module.exports = router;
