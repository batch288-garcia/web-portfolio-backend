const express = require("express");
const projectController = require("../contorllers/projectsContoller.js");

const router = express.Router();

// Route for adding Project
router.post("/AddProject", projectController.addProject);

// Router to Retrive active projects
router.get("/GetActiveProjects", projectController.getActiveProjects);

// Router to Retrieve Deactivated projects
router.get("/GetDeactivatedProjects", projectController.getDeactivatedProjects);

// Router for Retrieving Project
router.get("/GetProjects", projectController.retrieveProject);

// Router for Updating Project Details
router.patch("/:projectId/UpdateProject", projectController.updateProject);

// Router for Archiving Project
router.patch("/:projectId/ArchiveProject", projectController.archiveProject);

// Router for Un-Archive Project
router.patch(
  "/:projectId/UnArchiveProject",
  projectController.unArchiveProject
);

module.exports = router;
