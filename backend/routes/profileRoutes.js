const express = require("express");
const profileController = require("../contorllers/profileController");

const router = express.Router();

//Route for add profile detail
router.post("/AddProfileDetails", profileController.addProfile);

// Route for Get Active Profile
router.get("/GetActiveProfile", profileController.getActiveProfile);

// Route for Get Deactive Profile
router.get("/GetDeactiveProfile", profileController.getDeacticatedProfile);

// Route for Get all Profile
router.get("/GetProfiles", profileController.getProfiles);

// Route for Updating Profile Details
router.patch(
  "/:profileId/UpdateProfileDetails",
  profileController.updateProfileDetails
);

router.patch("/:profileId/ArchiveProfile", profileController.archiveProfile);

router.patch(
  "/:profileId/UnArchiveProfile",
  profileController.unArchiveProfile
);

module.exports = router;
